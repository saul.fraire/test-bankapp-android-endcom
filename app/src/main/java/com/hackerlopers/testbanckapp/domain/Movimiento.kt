package com.hackerlopers.testbanckapp.domain

data class MovimientoRequest (
    val movimientos: List<Movimiento>
)

data class Movimiento (
    val fecha: String,
    val descripcion: String,
    val monto: String,
    val tipo: String,
    val id: Long
)
