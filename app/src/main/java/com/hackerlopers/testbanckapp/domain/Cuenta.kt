package com.hackerlopers.testbanckapp.domain

data class CuentaRequest (
    val cuenta: List<Cuenta>
)

data class Cuenta (
    val cuenta: Long,
    val nombre: String,
    val ultimaSesion: String,
    val id: Long
)