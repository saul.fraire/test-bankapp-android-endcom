package com.hackerlopers.testbanckapp.domain

data class SaldoRequest (
    val saldos: List<Saldo>
)

data class Saldo (
    val cuenta: Long,
    val saldoGeneral: Long,
    val ingresos: Long,
    val gastos: Long,
    val id: Long
)