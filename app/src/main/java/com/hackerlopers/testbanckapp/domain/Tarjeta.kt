package com.hackerlopers.testbanckapp.domain

data class TarjetaRequest (
    val tarjetas: List<Tarjeta>
)

data class Tarjeta (
    val tarjeta: String,
    val nombre: String,
    val saldo: Long,
    val estado: String,
    val tipo: String,
    val id: Long
)