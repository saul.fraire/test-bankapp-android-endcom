package com.hackerlopers.testbanckapp.header.data

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class CuentaDataModule {

    @Provides
    fun getCuentaClientService(retrofit: Retrofit): CuentaRemoteService =
        retrofit.create(CuentaRemoteService::class.java)
}