package com.hackerlopers.testbanckapp.header.data

import com.hackerlopers.testbanckapp.domain.Cuenta

class CuentaRepository(private val cuentaRemoteService: CuentaRemoteService) {

    suspend fun getCuenta(): Cuenta {
        val result = cuentaRemoteService.getCuentaRequest()
        return result.cuenta[0]
    }
}