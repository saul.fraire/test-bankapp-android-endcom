package com.hackerlopers.testbanckapp.header.data

import com.hackerlopers.testbanckapp.domain.CuentaRequest
import retrofit2.http.GET

interface CuentaRemoteService {

    @GET("cuenta")
    suspend fun getCuentaRequest(): CuentaRequest

}