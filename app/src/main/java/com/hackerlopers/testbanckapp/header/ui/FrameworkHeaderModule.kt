package com.hackerlopers.testbanckapp.header.ui

import com.hackerlopers.testbanckapp.header.data.CuentaRemoteService
import com.hackerlopers.testbanckapp.header.data.CuentaRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(ViewModelComponent::class)
class FrameworkHeaderModule {

    @Provides
    fun getCuentaRepositoryProvider(cuentaRemoteService: CuentaRemoteService): CuentaRepository {
        return CuentaRepository(cuentaRemoteService)
    }
}