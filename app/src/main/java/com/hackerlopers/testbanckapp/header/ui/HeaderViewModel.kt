package com.hackerlopers.testbanckapp.header.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hackerlopers.testbanckapp.domain.Cuenta
import com.hackerlopers.testbanckapp.header.data.CuentaRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HeaderViewModel @Inject constructor(private val cuentaRepository: CuentaRepository) : ViewModel() {

    private val _progress = MutableLiveData<Boolean>()
    val progress: LiveData<Boolean> get() = _progress

    private val _cuenta = MutableLiveData<Cuenta>()
    val cuenta: LiveData<Cuenta> get() = _cuenta

    fun onLoadCuenta() {
        viewModelScope.launch(Dispatchers.Main) {
            _progress.value = true
            _cuenta.value = cuentaRepository.getCuenta()
            _progress.value = false
        }
    }
}