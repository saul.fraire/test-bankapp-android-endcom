package com.hackerlopers.testbanckapp.form.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.hackerlopers.testbanckapp.domain.Tarjeta

class FormViewModel: ViewModel() {
    private val _objJson = MutableLiveData<String>()
    val objJson: LiveData<String> get() = _objJson


    fun onGetJsonObj(nuevaTarjeta: Tarjeta) {
        //TODO: Generar objeto json con el formulario
        val gSon = Gson()
        _objJson.value = gSon.toJson(nuevaTarjeta)
    }
}