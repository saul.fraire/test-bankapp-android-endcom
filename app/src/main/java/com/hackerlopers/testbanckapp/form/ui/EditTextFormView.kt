package com.hackerlopers.testbanckapp.form.ui

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import com.hackerlopers.testbanckapp.R

class EditTextFormView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : AppCompatEditText(context, attrs) {

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        var color = ContextCompat.getColor(context, R.color.custom_gray_6)
        if (focused) color = ContextCompat.getColor(context, R.color.custom_green_3)
        setHintTextColor(color)
    }
}