package com.hackerlopers.testbanckapp.form.ui

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.hackerlopers.testbanckapp.R
import com.hackerlopers.testbanckapp.databinding.ActivityFormBinding
import com.hackerlopers.testbanckapp.domain.Tarjeta

class FormActivity : AppCompatActivity() {

    private val viewModel by viewModels<FormViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityFormBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val toolbar = findViewById<Toolbar>(R.id.customToolbarBack)
        setSupportActionBar(toolbar)


        binding.formToolbar.imageButton.setOnClickListener {
            finish()
        }

        binding.agregarButton.setOnClickListener {
            val nuevaTarjeta = Tarjeta(
                binding.numCardEditText.text.toString(),
                binding.nomCardEditText.text.toString(),
                binding.saldoEditText.text.toString().toLong(),
                binding.estatusEditText.text.toString(),
                binding.tipoCuentaEditText.text.toString(),
                123
            )
            viewModel.onGetJsonObj(nuevaTarjeta)
        }

        viewModel.objJson.observe(this) { objJson ->
            Log.e("objJson", objJson)
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Form Json")
            builder.setMessage(objJson)
            builder.setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })
            val alert = builder.create()
            alert.show()
        }
    }
}