package com.hackerlopers.testbanckapp.cards.ui

import com.hackerlopers.testbanckapp.cards.data.TarjetaRemoteService
import com.hackerlopers.testbanckapp.cards.data.TarjetaRepository
import com.hackerlopers.testbanckapp.header.data.CuentaRemoteService
import com.hackerlopers.testbanckapp.header.data.CuentaRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class FrameworkCardsModule{

    @Provides
    fun getTarjetaRepositoryProvider(tarjetaRemoteService: TarjetaRemoteService): TarjetaRepository {
        return TarjetaRepository(tarjetaRemoteService)
    }
}