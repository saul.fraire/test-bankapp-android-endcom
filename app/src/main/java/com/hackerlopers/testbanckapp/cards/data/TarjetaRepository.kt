package com.hackerlopers.testbanckapp.cards.data

import com.hackerlopers.testbanckapp.domain.Tarjeta

class TarjetaRepository(private val tarjetaRemoteService: TarjetaRemoteService) {
    suspend fun getTarjetas(): List<Tarjeta> {
        return tarjetaRemoteService.getCuentaRequest().tarjetas
    }
}