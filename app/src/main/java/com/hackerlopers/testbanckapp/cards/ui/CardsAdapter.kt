package com.hackerlopers.testbanckapp.cards.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hackerlopers.testbanckapp.R
import com.hackerlopers.testbanckapp.domain.Tarjeta

class CardsAdapter(private val cards: List<Tarjeta>): RecyclerView.Adapter<CardsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.view_item_card, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val card = cards[position]
        holder.bind(card)
    }

    override fun getItemCount(): Int = cards.size

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val imageCard = view.findViewById<ImageView>(R.id.image_card_view)
        private val status = view.findViewById<TextView>(R.id.status_text)
        private val amount = view.findViewById<TextView>(R.id.card_amount_text)
        private val cardNum = view.findViewById<TextView>(R.id.card_num_text)
        private val userName = view.findViewById<TextView>(R.id.user_name_text)
        private val holder = view.findViewById<TextView>(R.id.holder_text)

        fun bind(card: Tarjeta) {
            if (card.estado == "desactivada") imageCard.setImageResource(R.drawable.ic_card_inactive)
            status.text = card.estado
            amount.text = "\$${card.saldo}.00"
            cardNum.text = card.tarjeta
            userName.text = card.nombre
            holder.text = card.tipo
        }
    }
}