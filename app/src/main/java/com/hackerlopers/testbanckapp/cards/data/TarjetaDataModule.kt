package com.hackerlopers.testbanckapp.cards.data

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class TarjetaDataModule {

    @Provides
    fun getTarjetaClientService(retrofit: Retrofit): TarjetaRemoteService =
        retrofit.create(TarjetaRemoteService::class.java)
}