package com.hackerlopers.testbanckapp.cards.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hackerlopers.testbanckapp.cards.data.TarjetaRepository
import com.hackerlopers.testbanckapp.domain.Tarjeta
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CardsViewModel @Inject constructor(private val tarjetaRepository: TarjetaRepository): ViewModel() {

    private val _progress = MutableLiveData<Boolean>()
    val progress: LiveData<Boolean> get() = _progress

    private val _cards = MutableLiveData<List<Tarjeta>>()
    val cards: LiveData<List<Tarjeta>> get() = _cards

    fun onLoadTarjetas() {
        viewModelScope.launch(Dispatchers.Main) {
            _progress.value = true
            _cards.value = tarjetaRepository.getTarjetas()
            _progress.value = false
        }
    }
}