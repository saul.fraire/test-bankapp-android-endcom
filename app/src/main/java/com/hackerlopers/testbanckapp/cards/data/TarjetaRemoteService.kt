package com.hackerlopers.testbanckapp.cards.data

import com.hackerlopers.testbanckapp.domain.CuentaRequest
import com.hackerlopers.testbanckapp.domain.TarjetaRequest
import retrofit2.http.GET

interface TarjetaRemoteService {

    @GET("tarjetas")
    suspend fun getCuentaRequest(): TarjetaRequest
}