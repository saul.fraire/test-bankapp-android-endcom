package com.hackerlopers.testbanckapp.cards.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.hackerlopers.testbanckapp.R
import com.hackerlopers.testbanckapp.domain.Tarjeta

class ItemCardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    private val imageCard: ImageView
    private val status: TextView
    private val amount: TextView
    private val cardNum: TextView
    private val userName: TextView
    private val holder: TextView

    init {
        val view =
            LayoutInflater
                .from(context)
                .inflate(R.layout.view_item_card, this, true)

        imageCard = view.findViewById(R.id.image_card_view)
        status = view.findViewById(R.id.status_text)
        amount = view.findViewById(R.id.card_amount_text)
        cardNum = view.findViewById(R.id.card_num_text)
        userName = view.findViewById(R.id.user_name_text)
        holder = view.findViewById(R.id.holder_text)
    }

    fun setCardItem(tarjeta: Tarjeta) {
        //TODO: set image card!
        status.text = tarjeta.estado
        amount.text = "\$${tarjeta.saldo}.00"
        cardNum.text = tarjeta.tarjeta
        userName.text = tarjeta.nombre
        holder.text = tarjeta.tipo
    }
}