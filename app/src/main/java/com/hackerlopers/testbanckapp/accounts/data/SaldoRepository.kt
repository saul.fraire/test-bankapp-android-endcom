package com.hackerlopers.testbanckapp.accounts.data

import com.hackerlopers.testbanckapp.domain.Saldo

class SaldoRepository(private val saldoRemoteService: SaldoRemoteService) {
    suspend fun getSaldo(): Saldo {
        val result = saldoRemoteService.getSaldosRequest()
        return result.saldos[0]
    }
}