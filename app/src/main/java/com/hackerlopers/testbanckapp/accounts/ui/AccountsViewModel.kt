package com.hackerlopers.testbanckapp.accounts.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hackerlopers.testbanckapp.accounts.data.SaldoRepository
import com.hackerlopers.testbanckapp.domain.Saldo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AccountsViewModel @Inject constructor(private val saldoRepository: SaldoRepository): ViewModel() {

    private val _progress = MutableLiveData<Boolean>()
    val progress: LiveData<Boolean> get() = _progress

    private val _saldos = MutableLiveData<Saldo>()
    val saldo: LiveData<Saldo> get() = _saldos


    fun onLoadSaldos() {
        viewModelScope.launch(Dispatchers.Main) {
            _progress.value = true
            _saldos.value = saldoRepository.getSaldo()
            _progress.value = false
        }
    }
}