package com.hackerlopers.testbanckapp.accounts.data

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class SaldoDataModule {

    @Provides
    fun getsaldoClientService(retrofit: Retrofit): SaldoRemoteService =
        retrofit.create(SaldoRemoteService::class.java)
}