package com.hackerlopers.testbanckapp.accounts.data

import com.hackerlopers.testbanckapp.domain.SaldoRequest
import retrofit2.http.GET

interface SaldoRemoteService {
    @GET("saldos")
    suspend fun getSaldosRequest(): SaldoRequest
}