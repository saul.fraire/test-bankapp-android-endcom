package com.hackerlopers.testbanckapp.accounts.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.hackerlopers.testbanckapp.R
import com.hackerlopers.testbanckapp.databinding.FragmentAccountsBinding
import com.hackerlopers.testbanckapp.databinding.FragmentHeaderBinding
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AccountsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class AccountsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentAccountsBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<AccountsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAccountsBinding.inflate(inflater, container, false)
        viewModel.saldo.observe(viewLifecycleOwner) { saldos ->
            binding.saldoGeneral.setCard(
                "Saldo general en cuentas",
                "\$${saldos.saldoGeneral}.00"
            )
            binding.saldoIngresos.setCard(
                "Total de ingresos",
                "\$${saldos.ingresos}.00"
            )
            binding.sauldoGastos.setCard(
                "Total de gastos",
                "\$${saldos.gastos}.00"
            )
        }
        viewModel.onLoadSaldos()
        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AccountsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AccountsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}