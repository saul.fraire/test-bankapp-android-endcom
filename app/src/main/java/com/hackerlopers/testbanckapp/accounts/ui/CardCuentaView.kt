package com.hackerlopers.testbanckapp.accounts.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import com.hackerlopers.testbanckapp.R

class CardCuentaView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {
    private val titulo: TextView
    private val monto: TextView

    init {
        val view =
            LayoutInflater
                .from(context)
                .inflate(R.layout.view_card_cuenta, this, true)

        titulo = view.findViewById(R.id.cuenta_title)
        monto = view.findViewById(R.id.cuenta_monto)
    }

    fun setCard(title: String, amount: String){
        titulo.text = title
        monto.text = amount
    }
}