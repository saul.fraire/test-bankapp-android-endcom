package com.hackerlopers.testbanckapp.accounts.ui

import com.hackerlopers.testbanckapp.accounts.data.SaldoRemoteService
import com.hackerlopers.testbanckapp.accounts.data.SaldoRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class FrameworkSaldosModule {

    @Provides
    fun getCuentaRepositoryProvider(saldoRemoteService: SaldoRemoteService): SaldoRepository {
        return SaldoRepository(saldoRemoteService)
    }
}