package com.hackerlopers.testbanckapp.movements.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.hackerlopers.testbanckapp.R
import com.hackerlopers.testbanckapp.domain.Movimiento

class MovimientosAdapter(private val movs: List<Movimiento>): RecyclerView.Adapter<MovimientosAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.view_item_mov, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mov = movs[position]
        holder.bind(mov)
    }

    override fun getItemCount(): Int = movs.size

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val movName = view.findViewById<TextView>(R.id.mov_name_text)
        private val amountMov = view.findViewById<TextView>(R.id.amount_mov_text)
        private val dateText = view.findViewById<TextView>(R.id.date_text)
        var colorGreen = ContextCompat.getColor(view.context, R.color.custom_green_1)
        var colorRed = ContextCompat.getColor(view.context, R.color.red)
        fun bind(movimiento: Movimiento) {
            movName.text = movimiento.descripcion
            amountMov.text = "\$${movimiento.monto}"
            dateText.text = movimiento.fecha
            Log.e("Mov","${movimiento}")
            if (movimiento.tipo == "abono") {
                amountMov.setTextColor(colorGreen)
            } else {
                amountMov.setTextColor(colorRed)
            }
        }
    }
}