package com.hackerlopers.testbanckapp.movements.data

import com.hackerlopers.testbanckapp.domain.Movimiento

class MovimientoRepository(private val movimientoRemoteService: MovimientoRemoteService) {
    suspend fun getMovimientos(): List<Movimiento> {
        return movimientoRemoteService.getMovimientosRequest().movimientos
    }
}