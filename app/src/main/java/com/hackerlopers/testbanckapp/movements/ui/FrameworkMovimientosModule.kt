package com.hackerlopers.testbanckapp.movements.ui

import com.hackerlopers.testbanckapp.movements.data.MovimientoRemoteService
import com.hackerlopers.testbanckapp.movements.data.MovimientoRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent


@Module
@InstallIn(ViewModelComponent::class)
class FrameworkMovimientosModule {

    @Provides
    fun getMovimientoRepositoryProvider(movimientoRemoteService: MovimientoRemoteService): MovimientoRepository {
        return MovimientoRepository(movimientoRemoteService)
    }
}