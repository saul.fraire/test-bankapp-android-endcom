package com.hackerlopers.testbanckapp.movements.data

import com.hackerlopers.testbanckapp.domain.MovimientoRequest
import retrofit2.http.GET

interface MovimientoRemoteService {

    @GET("movimientos")
    suspend fun getMovimientosRequest(): MovimientoRequest
}