package com.hackerlopers.testbanckapp.movements.data

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class MovimientoDataModule {

    @Provides
    fun getMovimientoClientService(retrofit: Retrofit): MovimientoRemoteService =
        retrofit.create(MovimientoRemoteService::class.java)
}