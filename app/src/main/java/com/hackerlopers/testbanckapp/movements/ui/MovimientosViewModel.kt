package com.hackerlopers.testbanckapp.movements.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hackerlopers.testbanckapp.domain.Movimiento
import com.hackerlopers.testbanckapp.movements.data.MovimientoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovimientosViewModel @Inject constructor(private val movimientoRepository: MovimientoRepository): ViewModel() {
    private val _progress = MutableLiveData<Boolean>()
    val progress: LiveData<Boolean> get() = _progress

    private val _movs = MutableLiveData<List<Movimiento>>()
    val movs: LiveData<List<Movimiento>> get() = _movs

    fun onLoadMovimientos() {
        viewModelScope.launch(Dispatchers.Main) {
            _progress.value = true
            _movs.value = movimientoRepository.getMovimientos()
            _progress.value = false
        }
    }
}