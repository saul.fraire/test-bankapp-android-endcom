package com.hackerlopers.testbanckapp.movements.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import com.hackerlopers.testbanckapp.R
import com.hackerlopers.testbanckapp.domain.Movimiento

class ItemMovView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    private val movName: TextView
    private val amountMov: TextView
    private val dateText: TextView

    init {
        val view =
            LayoutInflater
                .from(context)
                .inflate(R.layout.view_item_mov, this, true)

        movName = view.findViewById(R.id.mov_name_text)
        amountMov = view.findViewById(R.id.amount_mov_text)
        dateText = view.findViewById(R.id.date_text)
    }

    fun setCardItem(movimiento: Movimiento) {
        movName.text = movimiento.descripcion
        amountMov.text = "\$${movimiento.monto}"
        dateText.text = movimiento.fecha
    }
}