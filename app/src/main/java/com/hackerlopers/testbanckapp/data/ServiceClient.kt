package com.hackerlopers.testbanckapp.data

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class ServiceClient {

    @Provides
    fun getRetrofitClientProvider(): Retrofit = Retrofit.Builder()
        .baseUrl("http://bankapp.endcom.mx/api/bankappTest/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}