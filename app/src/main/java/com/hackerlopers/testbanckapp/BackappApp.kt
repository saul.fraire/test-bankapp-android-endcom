package com.hackerlopers.testbanckapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BackappApp: Application()